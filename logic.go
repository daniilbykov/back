package main

import (
	"io/ioutil"
	"net/http"
	"net/url"
)

func getSitemap(url *url.URL) (string, error){
	sitemapURL := url.Scheme + "://" + url.Host + "/sitemap.xml"
	sitemapResp, err := http.Get(sitemapURL)
	if err != nil {
		return "", err
	}
	defer sitemapResp.Body.Close()
	sitemap, err:= ioutil.ReadAll(sitemapResp.Body)
	return string(sitemap), err
}

func getSource(url *url.URL) (string, error){
	sourceResp, err := http.Get(url.String())
	if err != nil {
		return "", err
	}
	defer sourceResp.Body.Close()
	source, err := ioutil.ReadAll(sourceResp.Body)
	return string(source), err
}

