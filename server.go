package main

import (
	"encoding/json"
	"log"
	"net/http"
	urlpkg "net/url"
)

type Server struct{}

func setupCORS(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func (server *Server) indexHandler(w http.ResponseWriter, req *http.Request) {
	setupCORS(&w, req)
	if (*req).Method == "OPTIONS" {
		return
	}

	w.Header().Set("Content-Type", "text")
	response, _ := json.Marshal("healthy")
	w.Write(response)
}

type MainActionOutput struct {
	Source  string `json:"source"`
	Sitemap string `json:"sitemap"`
}

type MainActionHtmlOutput struct {
	Source string `json:"source"`
}

type MainActionSitemapOutput struct {
	Sitemap string `json:"sitemap"`
}

func (server *Server) mainActionErrorResponse(w http.ResponseWriter, req *http.Request, err error) {
	w.Write([]byte(err.Error()))
}

func (server *Server) jsonResponse(w http.ResponseWriter, res interface{}) {
	response, _ := json.Marshal(res)
	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
}

func (server *Server) mainAction(w http.ResponseWriter, req *http.Request) {
	setupCORS(&w, req)
	if (*req).Method == "OPTIONS" {
		return
	}

	query := req.URL.Query()
	url := query.Get("url")

	u, err := urlpkg.Parse(url)
	if err != nil {
		server.mainActionErrorResponse(w, req, err)
		return
	}

	if len(url) == 0 {
		w.Write([]byte("URL is required\n"))
		return
	}

	content := query.Get("content")
	var res interface{}

	if content == "source" {
		source, _ := getSource(u)
		res = MainActionHtmlOutput{Source: source}
	} else if content == "sitemap" {
		sitemap, _ := getSitemap(u)
		res = MainActionSitemapOutput{Sitemap: sitemap}
	} else {
		source, _ := getSource(u)
		sitemap, _ := getSitemap(u)
		res = MainActionOutput{
			Source:  source,
			Sitemap: sitemap,
		}
	}
	server.jsonResponse(w, res)
}

func (server *Server) serve(port string) {

	http.HandleFunc("/", server.indexHandler)
	http.HandleFunc("/do", server.mainAction)

	log.Println("Listening on port " + port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
