package main

import (
	"log"

	"github.com/ilyakaznacheev/cleanenv"
)

var server Server
var config Config

type Config struct {
	Port   string `yaml:"port" env:"PORT" env-default:"80"`
}

func readConfig() {
	err := cleanenv.ReadConfig("config.yml", &config)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Config loaded")
}

func main() {
	log.Println("Starting server...")
	readConfig()
	server.serve(config.Port)
}
